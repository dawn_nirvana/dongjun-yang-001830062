/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Account;

import Role.Role;
import Role.Role.RoleType;

/**
 *
 * @author Administrator
 */
public class Account {
    private Person person;
    private String username;
    private String password;
    private Role role;
   
    public Account() {
        this.person=new Person();
    }

    public Person getPerson() {
        return person;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String toString(){
    return this.username;
    }
}
