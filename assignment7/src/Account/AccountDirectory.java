/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Account;

import Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class AccountDirectory {
    private ArrayList<Account> ad;

    public AccountDirectory() {
        this.ad=new ArrayList<>();
    }

    public ArrayList<Account> getAd() {
        return ad;
    }

    public void setAd(ArrayList<Account> ad) {
        this.ad = ad;
    }
     public Account authenticateUser(String username, String password){
        for (Account ua : ad)
            if (ua.getUsername().equals(username) && ua.getPassword().equals(password)){
                return ua;
            }
        return null;
    }
    
    public Account createUserAccount(String username, String password, Person employee, Role role){
       Account userAccount = new Account();
        userAccount.setUsername(username);
        userAccount.setPassword(password);
        userAccount.setPerson(employee);
        userAccount.setRole(role);
        ad.add(userAccount);
        return userAccount;
    }
    
    public boolean checkIfUsernameIsUnique(String username){
        for (Account  a: ad){
            if (a.getUsername().equals(username))
                return false;
        }
        return true;
    }
}
