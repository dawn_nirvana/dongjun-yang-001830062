/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Account.AccountDirectory;
import Account.PersonDirectory;
import Disease.DiseaseCatalog;
import Network.Network;
import Organization.Organization;
import Role.Role;
import Role.SystemAdminRole;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class EcoSystem extends Organization{
     private static EcoSystem business;
    private ArrayList<Network> networkList;
private PersonDirectory pd;
private AccountDirectory ad;
private DiseaseCatalog dc;
    public static EcoSystem getInstance() {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    private EcoSystem() {
        super(null);
        networkList = new ArrayList<>();
        this.pd=new PersonDirectory();
        this.ad=new AccountDirectory();
        this.dc=new DiseaseCatalog();
    }

    public DiseaseCatalog getDc() {
        return dc;
    }

    public void setDc(DiseaseCatalog dc) {
        this.dc = dc;
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public Network createAndAddNetwork() {
        Network network = new Network();
        networkList.add(network);
        return network;
    }

    public PersonDirectory getPd() {
        return pd;
    }

    public void setPd(PersonDirectory pd) {
        this.pd = pd;
    }

    public AccountDirectory getAd() {
        return ad;
    }

    public void setAd(AccountDirectory ad) {
        this.ad = ad;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }


    public boolean checkIfUsernameIsUnique(String username) {

        if (!this.getUserAccountDirectory().checkIfUsernameIsUnique(username)) {
            return false;
        }

//        for (Network network : networkList) {
//        }

        return true;
    }
}
