/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Account.Account;
import Account.Person;
import Disease.Disease;
import Disease.Disease.Rank;
import Enterprise.DistributorEnterprise;
import Enterprise.HospitalEnterprise;
import Enterprise.ProviderEnterprise;
import Network.Network;
import Organization.AdminOrganization;
import Organization.ClinicOrganization;
import Organization.SupplierOrganization;
import Role.AdminRole;
import Role.ClinicRole;
import Role.Role;
import Role.Role.RoleType;
import Role.SupplierRole;
import Role.SystemAdminRole;
import Vaccine.Vaccine;
import Vaccine.VaccineDirectory;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Administrator
 */
public class ConfigureEcoSystem {
     public static EcoSystem configure(){
        
        EcoSystem system = EcoSystem.getInstance();
        
        Network nw=new Network();
       //Create a network
       nw.setName("Perfect network");    
          HospitalEnterprise he=new HospitalEnterprise("MA Health Center");
          ProviderEnterprise pe1=new ProviderEnterprise("Best Biotechnology Company");
          ProviderEnterprise pe2=new ProviderEnterprise("Extending-Life");
          DistributorEnterprise de=new DistributorEnterprise("CVS Pharmacy");
//create an enterprise
        ClinicOrganization co1=new ClinicOrganization();
        AdminOrganization ao1=new AdminOrganization();
        SupplierOrganization sp1=new SupplierOrganization();
          AdminOrganization ao2=new AdminOrganization();
           SupplierOrganization sp2=new SupplierOrganization();
          AdminOrganization ao3=new AdminOrganization();
           String line = "";
        String cvsSplitBy = ",";
        BufferedReader br1 = null;
         String csvFile1 = "MAadmin.csv";
        try {

            br1= new BufferedReader(new FileReader(csvFile1));
            while ((line = br1.readLine()) != null) {
                String[] user = line.split(cvsSplitBy);
                
                String name = user[0];
                String license=user[1];
                String gender=user[2];
                String username=user[3];
                String key=user[4];
                Person p=ao1.getEmployeeDirectory().addPerson(name);
                p.setGender(gender);p.setLicense(license);
                AdminRole ar=new AdminRole();
                Account a=ao1.getUserAccountDirectory().createUserAccount(username, key, p, ar);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br1 != null) {
                try {
                    br1.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }
           BufferedReader br2 = null;
         String csvFile2 = "MAclinic.csv";
        try {

            br2= new BufferedReader(new FileReader(csvFile2));
            while ((line = br2.readLine()) != null) {
                String[] user = line.split(cvsSplitBy);
                
                String name = user[0];
                String license=user[1];
                String gender=user[2];
                String username=user[3];
                String key=user[4];
                Person p=ao1.getEmployeeDirectory().addPerson(name);
                p.setGender(gender);p.setLicense(license);
                ClinicRole ar=new ClinicRole();
                Account a=ao1.getUserAccountDirectory().createUserAccount(username, key, p, ar);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br2!= null) {
                try {
                    br2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }
         BufferedReader br3 = null;
         String csvFile3= "Disease.csv";
        try {

            br3= new BufferedReader(new FileReader(csvFile3));
            while ((line = br3.readLine()) != null) {
                String[] disease = line.split(cvsSplitBy);
                
                String name = disease[0];
                String rank=disease[1];
                Disease d=system.getDc().addDisease();
                d.setName(name);                
                d.setRank(rank);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br3!= null) {
                try {
                    br3.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }
        he.getOrganizationDirectory().getOd().add(ao1);
        he.getOrganizationDirectory().getOd().add(co1);
          BufferedReader br4 = null;
         String csvFile4= "BBC.csv";
        try {

            br4= new BufferedReader(new FileReader(csvFile4));
            while ((line = br3.readLine()) != null) {
                String[] disease = line.split(cvsSplitBy);
                
                String name = disease[1];
                String d=disease[0];
                String des=disease[2];
                String num=disease[3];
               VaccineDirectory v=sp1.getVc().addVaccineDirectory();
               v.getV().setDescription(des);
               v.getV().setName(name);
               v.setNumber(Integer.parseInt(num));
               for(Disease s:system.getDc().getDc())
                   if(s.getName().equals(d)){
                   v.getV().setDisease(s);break;
                   }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br4!= null) {
                try {
                    br4.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }
           BufferedReader br5 = null;
         String csvFile5= "EL.csv";
        try {

            br5= new BufferedReader(new FileReader(csvFile5));
            while ((line = br5.readLine()) != null) {
                String[] disease = line.split(cvsSplitBy);
                
                 String name = disease[1];
                String d=disease[0];
                String des=disease[2];
                String num=disease[3];
               VaccineDirectory v=sp2.getVc().addVaccineDirectory();
               v.getV().setDescription(des);
               v.getV().setName(name);
               v.setNumber(Integer.parseInt(num));
               for(Disease s:system.getDc().getDc())
                   if(s.getName().equals(d)){
                   v.getV().setDisease(s);break;
                   }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br5!= null) {
                try {
                    br5.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }
         Person p2=ao2.getEmployeeDirectory().addPerson("Andy");
                p2.setGender("male");p2.setLicense("6018");
                AdminRole ar1=new AdminRole();
                Account a1=ao2.getUserAccountDirectory().createUserAccount("BBCadmin", "BBCadmin", p2, ar1);
                 Person p4=ao2.getEmployeeDirectory().addPerson("Juan");
                p2.setGender("male");p2.setLicense("7754");
                SupplierRole ar3=new SupplierRole();
                Account a3=ao2.getUserAccountDirectory().createUserAccount("BBCsupplier", "BBCsupplier", p4, ar3);
                
                 Person p3=ao3.getEmployeeDirectory().addPerson("Julie");
                p2.setGender("female");p2.setLicense("6312");
                AdminRole ar2=new AdminRole();
                Account a2=ao3.getUserAccountDirectory().createUserAccount("ELadmin", "ELadmin", p3, ar2);
                
                pe1.getOrganizationDirectory().getOd().add(sp1);
                pe1.getOrganizationDirectory().getOd().add(ao2);
                pe2.getOrganizationDirectory().getOd().add(sp2);
                pe2.getOrganizationDirectory().getOd().add(ao3);
        //initialize some organizations
        //have some employees 
        //create user account
        
        
        Person employee = system.getPd().addPerson("RRH");
        
        Account ua = system.getAd().createUserAccount("sysadmin", "sysadmin", employee, new SystemAdminRole());
        
        return system;
    }
}
