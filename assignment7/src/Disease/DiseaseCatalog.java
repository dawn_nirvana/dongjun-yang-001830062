/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Disease;

import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class DiseaseCatalog {
 private ArrayList<Disease> dc;

    public DiseaseCatalog() {
        this.dc=new ArrayList<>();
        
    }

    public ArrayList<Disease> getDc() {
        return dc;
    }

    public void setDc(ArrayList<Disease> dc) {
        this.dc = dc;
    }
 
    public Disease addDisease(){
    Disease a=new Disease();
    dc.add(a);
    return a;
    }
    
    public void deleteDisease(Disease d){
    for(Disease a:dc){
    if(a.getName().equals(d.getName())){dc.remove(a);break;}
    }
    }
}
