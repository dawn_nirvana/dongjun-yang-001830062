/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Role;

import Account.Account;
import Business.EcoSystem;
import Enterprise.Enterprise;
import InterfaceSupplier.SupplierWorkAreaJPanel;

import Organization.Organization;
import javax.swing.JPanel;

/**
 *
 * @author Administrator
 */
public class SupplierRole extends Role{
     public  JPanel createWorkArea(JPanel userProcessContainer, 
            Account account, 
            Organization organization, 
            Enterprise enterprise, 
            EcoSystem business){
     return new SupplierWorkAreaJPanel(userProcessContainer, account, organization, business);
   }
}
