/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Role;

import Account.Account;
import Business.EcoSystem;
import Enterprise.Enterprise;
import Organization.Organization;
import javax.swing.JPanel;

/**
 *
 * @author Administrator
 */
public abstract class Role {
      public enum RoleType{
        Admin("Admin"),
        Clinic("Clinic"),
        Supplier("Supplier"),
        SystemAdmin("System Admin");
        private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }
         @Override
        public String toString() {
            return value;
        }
      }
    
      public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            Account account, 
            Organization organization, 
            Enterprise enterprise, 
            EcoSystem business);
      
      public String toString(){
      return this.getClass().getName();
      }
}
