/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vaccine;

/**
 *
 * @author Administrator
 */
public class VaccineDirectory {
    private Vaccine v;
    private int number;

    public VaccineDirectory() {
        this.v=new Vaccine();
    }

    public Vaccine getV() {
        return v;
    }

    public void setV(Vaccine v) {
        this.v = v;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    
}
