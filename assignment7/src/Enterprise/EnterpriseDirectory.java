/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enterprise;

import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class EnterpriseDirectory {
    private ArrayList<Enterprise> ed;   

    public ArrayList<Enterprise> getEd() {
        return ed;
    }

    public void setEd(ArrayList<Enterprise> ed) {
        this.ed = ed;
    }

    public EnterpriseDirectory() {
        this.ed=new ArrayList<>();
    }
    
    public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type){
        Enterprise enterprise = null;
        if (type == Enterprise.EnterpriseType.Hospital){
            enterprise = new HospitalEnterprise(name);
            ed.add(enterprise);
        }else  if (type == Enterprise.EnterpriseType.Provider){
            enterprise = new ProviderEnterprise(name);
            ed.add(enterprise);
        }else  if (type == Enterprise.EnterpriseType.Distributor){
            enterprise = new DistributorEnterprise(name);
            ed.add(enterprise);
        }
        return enterprise;
    }
}
