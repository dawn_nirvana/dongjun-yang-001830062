/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Organization;

import Role.Role;
import Role.SupplierRole;
import Vaccine.VaccineCatalog;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class SupplierOrganization extends Organization {
    private VaccineCatalog vc;
 
    public SupplierOrganization() {
        super(Organization.Type.Supplier.getValue());
        this.vc=new VaccineCatalog();
    }
       @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new SupplierRole());
        return roles;
    }

    public VaccineCatalog getVc() {
        return vc;
    }

    public void setVc(VaccineCatalog vc) {
        this.vc = vc;
    }
    
}
