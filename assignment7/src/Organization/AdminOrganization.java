/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Organization;

import Disease.DiseaseCatalog;
import Role.AdminRole;
import Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class AdminOrganization extends Organization{
    private DiseaseCatalog dc;
    public AdminOrganization() {
        super(Organization.Type.Admin.getValue());
        this.dc=new DiseaseCatalog();
    }
       @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new AdminRole());
        return roles;
    }
}
