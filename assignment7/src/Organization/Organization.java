/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Organization;

import Account.AccountDirectory;
import Account.PersonDirectory;
import Order.OrderList;
import Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public  abstract class Organization {
       private String name;
    private PersonDirectory employeeDirectory;
    private AccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter;
    private OrderList ol;
    public  enum Type{
        Admin("Admin Organization"), Clinic("Clinic Organization"), Supplier("Supplier Organization"),SystemAdmin
                ("System Admin");
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
    
    public Organization(String name) {
        this.name = name;
        employeeDirectory = new PersonDirectory();
        userAccountDirectory = new AccountDirectory();
        ol=new OrderList();
        organizationID = counter;
        ++counter;
    }
    public abstract ArrayList<Role> getSupportedRole();

    public OrderList getOl() {
        return ol;
    }

    public void setOl(OrderList ol) {
        this.ol = ol;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersonDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(PersonDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    public AccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(AccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(int organizationID) {
        this.organizationID = organizationID;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Organization.counter = counter;
    }
    
    public String toString(){
    return this.name;
    }
}
