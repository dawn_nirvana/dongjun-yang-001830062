/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Organization;

import java.util.ArrayList;
import Organization.Organization.Type;
        
/**
 *
 * @author Administrator
 */
public class OrganizationDirectory {
    private ArrayList<Organization> od;

    public ArrayList<Organization> getOd() {
        return od;
    }

    public void setOd(ArrayList<Organization> od) {
        this.od = od;
    }

    public OrganizationDirectory() {
        this.od=new ArrayList<>();
    }
    public Organization addOrganization(Type type){
         Organization organization = null;
        if (type.getValue().equals(Type.Clinic.getValue())){
            organization = new ClinicOrganization();
            od.add(organization);
        }
        else if (type.getValue().equals(Type.Supplier.getValue())){
            organization = new SupplierOrganization();
            od.add(organization);
        }
        return organization;
    }
}
