/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Organization;

import Role.ClinicRole;
import Role.Role;
import Vaccine.VaccineCatalog;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class ClinicOrganization extends Organization {
    private VaccineCatalog vc;
    public ClinicOrganization() {
        super(Organization.Type.Clinic.getValue());
        this.vc=new VaccineCatalog();
    }
       @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new ClinicRole());
        return roles;
    }

    public VaccineCatalog getVc() {
        return vc;
    }

    public void setVc(VaccineCatalog vc) {
        this.vc = vc;
    }
    
}
