/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;
import Business.User;
import Business.UserDirectory;
import Customer.*;
import Airliner.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author Administrator
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     */
    private UserDirectory ud;
     private AirlinerDirectory ad;
     private static int wh;
     private CustomerDirectory cd;
     DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm"); 
    public MainFrame() {
               initComponents();
        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        jButton4.setEnabled(false);
        ud=new UserDirectory();
        ad=new AirlinerDirectory();
        cd=new CustomerDirectory();
          String csvFile = "users.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] airplaneElement = line.split(cvsSplitBy);
                User u=ud.addUser();
                String name = airplaneElement[0];
                String key =airplaneElement[1];
                String pre=airplaneElement[2];
                u.setUsername(name);
                u.setPassword(key);
                u.setPrivilege(pre);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }
        for(User u1:ud.getUd()){
        jComboBox1.addItem(u1.getUsername());
        }
        
         String csvFileair = "airliner.csv";
        BufferedReader brair = null;
        try {

            brair = new BufferedReader(new FileReader(csvFileair));
            while ((line = brair.readLine()) != null) {
                String[] airlinerElement = line.split(cvsSplitBy);
                Airliner a1=ad.addAirliner();
                String name = airlinerElement[0];
                String phone =airlinerElement[1];
                String country=airlinerElement[2];
                a1.setName(name);
                a1.setPhone(phone);
                a1.setCountry(country);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }
             for(Airliner a:ad.getAd()){
                 String s1="American Airline";
             String s2="Singapore";
             String s3="Virgin";
                    if (a.getName().equals(s1)){
            String csvFile1= "flightofAA.csv";
                      BufferedReader br1 = null;
                    try {
             br1 = new BufferedReader(new FileReader(csvFile1));
            while ((line = br1.readLine()) != null) {
                String[] airplaneElement = line.split(cvsSplitBy);
                Flight f1=a.getFl().addFlight();
                int fleet = Integer.parseInt(airplaneElement[0]);
                String flight=airplaneElement[1];
                Date take=df.parse(airplaneElement[2]);
                String air=airplaneElement[3];
                String des=airplaneElement[4];
                f1.setAirport(air);
                f1.setDestination(des);
                f1.setTakeoff(take);
                f1.setFleetno(fleet);
                f1.setFlightno(flight);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }       catch (ParseException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
            if (br1 != null) {
                try {
                    br1.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }}
        }
            if (a.getName().equals(s2)){
             String csvFile1="flightofS.csv";
              BufferedReader br2 = null;
                    try {
            br2 = new BufferedReader(new FileReader(csvFile1));
            while ((line = br2.readLine()) != null) {
                String[] airplaneElement = line.split(cvsSplitBy);
                Flight f1=a.getFl().addFlight();
                int fleet = Integer.parseInt(airplaneElement[0]);
                String flight=airplaneElement[1];
                Date take=new Date();
                try {
                   take=df.parse(airplaneElement[2]);
                } catch (ParseException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                String air=airplaneElement[3];
                String des=airplaneElement[4];
                f1.setAirport(air);
                f1.setDestination(des);
                f1.setTakeoff(take);
                f1.setFleetno(fleet);
                f1.setFlightno(flight);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br2 != null) {
                try {
                    br2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }}}
            if (a.getName().equals(s3)){
             String  csvFile1="flightofV.csv";
              BufferedReader br3= null;
                    try { 
            br3= new BufferedReader(new FileReader(csvFile1));
            while ((line = br3.readLine()) != null) {
                String[] airplaneElement = line.split(cvsSplitBy);
                int fleet = Integer.parseInt(airplaneElement[0]);
                String flight=airplaneElement[1];
                  Date take=df.parse(airplaneElement[2]);
                String air=airplaneElement[3];
                String des=airplaneElement[4];
                Flight f1=a.getFl().addFlight();
                f1.setAirport(air);
                f1.setDestination(des);
                f1.setTakeoff(take);
                f1.setFleetno(fleet);
                f1.setFlightno(flight);
            } 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }            catch (ParseException ex) {
                         Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                     } finally {
            if (br3 != null) {
                try {
                    br3.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }}}
        }
    String csvFilec = "customer2.csv";
        BufferedReader brc = null;
        try {

            brc = new BufferedReader(new FileReader(csvFilec));
            while ((line = brc.readLine()) != null) {
                
              String[] customer = line.split(cvsSplitBy);
                    Customer cm2 = cd.addCustomer();
                String id = customer[0];
                String uname =customer[1];                                
                String name=customer[2];
                int age=Integer.parseInt(customer[3]);
                String sex=customer[4];
                String add=customer[5];
                String phone=customer[6];
                String license=customer[7];
                cm2.setAccountno(id);
                cm2.setAccountname(uname);
                cm2.getP().setAddress(add);
             cm2.getP().setAge(age);
             cm2.getP().setLicenseid(license);
             cm2.getP().setName(name);
             cm2.getP().setPhone(phone);
             cm2.getP().setSex(sex);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }
    
    
    
    
    }     

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        startb = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        searchforTA = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        flight2 = new javax.swing.JTextField();
        dep2 = new javax.swing.JTextField();
        des2 = new javax.swing.JTextField();
        air = new javax.swing.JTextField();
        jButton18 = new javax.swing.JButton();
        jButton19 = new javax.swing.JButton();
        travelagency = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        Update = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        flightno = new javax.swing.JTextField();
        fleetno = new javax.swing.JTextField();
        takeoff = new javax.swing.JTextField();
        dep = new javax.swing.JTextField();
        des = new javax.swing.JTextField();
        jButton13 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        addFlight = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        flight1 = new javax.swing.JTextField();
        fleet1 = new javax.swing.JTextField();
        take1 = new javax.swing.JTextField();
        dep1 = new javax.swing.JTextField();
        des1 = new javax.swing.JTextField();
        jButton16 = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        getnameTX = new javax.swing.JLabel();
        FlightMain = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        Create = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        airname = new javax.swing.JTextField();
        airphone = new javax.swing.JTextField();
        aircountry = new javax.swing.JTextField();
        jButton22 = new javax.swing.JButton();
        jButton23 = new javax.swing.JButton();
        AirlinerMain = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton5 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();
        Personal = new javax.swing.JPanel();
        selectseat = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTable8 = new javax.swing.JTable();
        jButton12 = new javax.swing.JButton();
        jButton28 = new javax.swing.JButton();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        Book = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTable7 = new javax.swing.JTable();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        dep3 = new javax.swing.JTextField();
        des3 = new javax.swing.JTextField();
        air3 = new javax.swing.JTextField();
        lt = new javax.swing.JTextField();
        ht = new javax.swing.JTextField();
        jButton26 = new javax.swing.JButton();
        jButton27 = new javax.swing.JButton();
        jLabel28 = new javax.swing.JLabel();
        CustomerMain = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable6 = new javax.swing.JTable();
        jButton24 = new javax.swing.JButton();
        jButton25 = new javax.swing.JButton();
        loginPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        key = new javax.swing.JPasswordField();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton2.setText("Airliner Management");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Customer Management");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Travel-Agency Management");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        startb.setText("Start");
        startb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startbActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton4)
                    .addComponent(jButton3)
                    .addComponent(startb)
                    .addComponent(jButton2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(startb)
                .addGap(48, 48, 48)
                .addComponent(jButton2)
                .addGap(41, 41, 41)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 105, Short.MAX_VALUE)
                .addComponent(jButton4)
                .addGap(60, 60, 60))
        );

        jSplitPane2.setLeftComponent(jPanel3);

        jPanel4.setLayout(new java.awt.CardLayout());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 567, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 379, Short.MAX_VALUE)
        );

        jPanel4.add(jPanel1, "card3");

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Flight number", "Departure", "Destination", "Airliner", "Take off"
            }
        ));
        jScrollPane5.setViewportView(jTable5);

        jLabel16.setText("Flight number:");

        jLabel17.setText("Departure:");

        jLabel19.setText("Airliner:");

        jLabel20.setText("Destination:");

        jButton18.setText("Previous");
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });

        jButton19.setText("Search");
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout searchforTALayout = new javax.swing.GroupLayout(searchforTA);
        searchforTA.setLayout(searchforTALayout);
        searchforTALayout.setHorizontalGroup(
            searchforTALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(searchforTALayout.createSequentialGroup()
                .addGroup(searchforTALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(searchforTALayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(searchforTALayout.createSequentialGroup()
                        .addGap(117, 117, 117)
                        .addComponent(jButton18)))
                .addContainerGap(83, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, searchforTALayout.createSequentialGroup()
                .addGroup(searchforTALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(searchforTALayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton19))
                    .addGroup(searchforTALayout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addGroup(searchforTALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel17)
                            .addComponent(jLabel16))
                        .addGap(39, 39, 39)
                        .addGroup(searchforTALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(flight2, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)
                            .addComponent(dep2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(searchforTALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19)
                            .addComponent(jLabel20))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(searchforTALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(des2, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                    .addComponent(air))
                .addGap(69, 69, 69))
        );
        searchforTALayout.setVerticalGroup(
            searchforTALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, searchforTALayout.createSequentialGroup()
                .addContainerGap(29, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addGroup(searchforTALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jLabel19)
                    .addComponent(flight2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(air, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addGroup(searchforTALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jLabel20)
                    .addComponent(dep2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(des2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(searchforTALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton18)
                    .addComponent(jButton19))
                .addGap(55, 55, 55))
        );

        jPanel4.add(searchforTA, "card8");

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Flight number", "Airliner", "Departure", "Destination", "Take off "
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        jButton6.setText("Show all flights");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setText("Conditional search");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout travelagencyLayout = new javax.swing.GroupLayout(travelagency);
        travelagency.setLayout(travelagencyLayout);
        travelagencyLayout.setHorizontalGroup(
            travelagencyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, travelagencyLayout.createSequentialGroup()
                .addContainerGap(178, Short.MAX_VALUE)
                .addGroup(travelagencyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(travelagencyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButton7)
                        .addComponent(jButton6)))
                .addContainerGap())
        );
        travelagencyLayout.setVerticalGroup(
            travelagencyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, travelagencyLayout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addComponent(jButton6)
                .addGap(44, 44, 44)
                .addComponent(jButton7)
                .addGap(90, 90, 90))
        );

        jPanel4.add(travelagency, "card6");

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Flight number", "Fleet number", "Departure", "Destination", "Take off"
            }
        ));
        jScrollPane4.setViewportView(jTable4);

        jLabel6.setText("Flight number:");

        jLabel7.setText("Fleet number:");

        jLabel8.setText("Take off time:");

        jLabel9.setText("Departure:");

        jLabel10.setText("Destination:");

        jButton13.setText("Previous");
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        jButton14.setText("Update");
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout UpdateLayout = new javax.swing.GroupLayout(Update);
        Update.setLayout(UpdateLayout);
        UpdateLayout.setHorizontalGroup(
            UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(UpdateLayout.createSequentialGroup()
                .addGroup(UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(UpdateLayout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(UpdateLayout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addGroup(UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(UpdateLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(30, 30, 30)
                                .addComponent(flightno, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE))
                            .addGroup(UpdateLayout.createSequentialGroup()
                                .addGroup(UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8))
                                .addGap(30, 30, 30)
                                .addGroup(UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(takeoff)
                                    .addComponent(fleetno))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10))
                        .addGap(54, 54, 54)
                        .addGroup(UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dep, javax.swing.GroupLayout.DEFAULT_SIZE, 72, Short.MAX_VALUE)
                            .addComponent(des)))
                    .addGroup(UpdateLayout.createSequentialGroup()
                        .addGap(89, 89, 89)
                        .addComponent(jButton13)
                        .addGap(138, 138, 138)
                        .addComponent(jButton14)))
                .addContainerGap(72, Short.MAX_VALUE))
        );
        UpdateLayout.setVerticalGroup(
            UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, UpdateLayout.createSequentialGroup()
                .addContainerGap(70, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addGroup(UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel9)
                    .addComponent(flightno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel10)
                    .addComponent(fleetno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(des, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(takeoff, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(UpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton13)
                    .addComponent(jButton14))
                .addGap(23, 23, 23))
        );

        jPanel4.add(Update, "card9");

        jLabel11.setText("Flight number:");

        jLabel12.setText("Fleet number:");

        jLabel13.setText("Take off time:");

        jLabel14.setText("Departure:");

        jLabel15.setText("Destination:");

        jButton16.setText("Previous");
        jButton16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton16ActionPerformed(evt);
            }
        });

        jButton17.setText("Add");
        jButton17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton17ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout addFlightLayout = new javax.swing.GroupLayout(addFlight);
        addFlight.setLayout(addFlightLayout);
        addFlightLayout.setHorizontalGroup(
            addFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addFlightLayout.createSequentialGroup()
                .addGroup(addFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(addFlightLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(addFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(addFlightLayout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(flight1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(addFlightLayout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(take1))
                            .addGroup(addFlightLayout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addGap(18, 18, 18)
                                .addComponent(fleet1))))
                    .addGroup(addFlightLayout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(jButton16)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 126, Short.MAX_VALUE)
                .addGroup(addFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(addFlightLayout.createSequentialGroup()
                        .addComponent(jButton17)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(addFlightLayout.createSequentialGroup()
                        .addGroup(addFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(jLabel15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                        .addGroup(addFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dep1, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                            .addComponent(des1))
                        .addGap(0, 49, Short.MAX_VALUE))))
            .addGroup(addFlightLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(getnameTX)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        addFlightLayout.setVerticalGroup(
            addFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addFlightLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(getnameTX)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(addFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel14)
                    .addComponent(flight1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dep1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(addFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel15)
                    .addComponent(fleet1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(des1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(addFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(take1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 172, Short.MAX_VALUE)
                .addGroup(addFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton16)
                    .addComponent(jButton17))
                .addGap(37, 37, 37))
        );

        jPanel4.add(addFlight, "card10");

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Flight number", "Fleet number", "Departure", "Destination", "Take off"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        jLabel5.setText("Flight");

        jButton8.setText("Update");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setText("View Customer");

        jButton10.setText("Previous");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton11.setText("Add Flight");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton15.setText("Search");
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });

        jButton20.setText("Cancel Flight");
        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FlightMainLayout = new javax.swing.GroupLayout(FlightMain);
        FlightMain.setLayout(FlightMainLayout);
        FlightMainLayout.setHorizontalGroup(
            FlightMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FlightMainLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(FlightMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FlightMainLayout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 512, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 15, Short.MAX_VALUE))
                    .addGroup(FlightMainLayout.createSequentialGroup()
                        .addGroup(FlightMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton10)
                            .addComponent(jButton15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(FlightMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton11, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(105, 105, 105)
                        .addGroup(FlightMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton20)
                            .addComponent(jButton9))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FlightMainLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addGap(244, 244, 244))
        );
        FlightMainLayout.setVerticalGroup(
            FlightMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FlightMainLayout.createSequentialGroup()
                .addContainerGap(90, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addGap(36, 36, 36)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(53, 53, 53)
                .addGroup(FlightMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton11)
                    .addComponent(jButton15)
                    .addComponent(jButton20))
                .addGap(18, 18, 18)
                .addGroup(FlightMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton8)
                    .addComponent(jButton9)
                    .addComponent(jButton10))
                .addGap(41, 41, 41))
        );

        jPanel4.add(FlightMain, "card7");

        jLabel18.setText("Name:");

        jLabel21.setText("Phone:");

        jLabel22.setText("Country:");

        jButton22.setText("Create");
        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });

        jButton23.setText("Previous");
        jButton23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton23ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout CreateLayout = new javax.swing.GroupLayout(Create);
        Create.setLayout(CreateLayout);
        CreateLayout.setHorizontalGroup(
            CreateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CreateLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(CreateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(jLabel21)
                    .addComponent(jLabel22))
                .addGap(49, 49, 49)
                .addGroup(CreateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(airname, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                    .addComponent(airphone)
                    .addComponent(aircountry))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CreateLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jButton23)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 283, Short.MAX_VALUE)
                .addComponent(jButton22)
                .addGap(85, 85, 85))
        );
        CreateLayout.setVerticalGroup(
            CreateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CreateLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(CreateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(airname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addGroup(CreateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(airphone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39)
                .addGroup(CreateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(aircountry, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 111, Short.MAX_VALUE)
                .addGroup(CreateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton22)
                    .addComponent(jButton23))
                .addGap(64, 64, 64))
        );

        jPanel4.add(Create, "card11");

        jLabel4.setText("AirlinerManagement");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Airliner", "Phone", "Country"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jButton5.setText("View Flight");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton21.setText("Create AirLiner");

        javax.swing.GroupLayout AirlinerMainLayout = new javax.swing.GroupLayout(AirlinerMain);
        AirlinerMain.setLayout(AirlinerMainLayout);
        AirlinerMainLayout.setHorizontalGroup(
            AirlinerMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AirlinerMainLayout.createSequentialGroup()
                .addGroup(AirlinerMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AirlinerMainLayout.createSequentialGroup()
                        .addGap(152, 152, 152)
                        .addComponent(jLabel4)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AirlinerMainLayout.createSequentialGroup()
                        .addContainerGap(178, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AirlinerMainLayout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jButton21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton5)
                .addGap(62, 62, 62))
        );
        AirlinerMainLayout.setVerticalGroup(
            AirlinerMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AirlinerMainLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 135, Short.MAX_VALUE)
                .addGroup(AirlinerMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton5)
                    .addComponent(jButton21))
                .addGap(61, 61, 61))
        );

        jPanel4.add(AirlinerMain, "card4");

        javax.swing.GroupLayout PersonalLayout = new javax.swing.GroupLayout(Personal);
        Personal.setLayout(PersonalLayout);
        PersonalLayout.setHorizontalGroup(
            PersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        PersonalLayout.setVerticalGroup(
            PersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jPanel4.add(Personal, "card12");

        jTable8.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Number", "Price", "Kind", "Status"
            }
        ));
        jScrollPane8.setViewportView(jTable8);

        jButton12.setText("Book");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jButton28.setText("Previous");
        jButton28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton28ActionPerformed(evt);
            }
        });

        jLabel29.setText("Total seats remain:");

        javax.swing.GroupLayout selectseatLayout = new javax.swing.GroupLayout(selectseat);
        selectseat.setLayout(selectseatLayout);
        selectseatLayout.setHorizontalGroup(
            selectseatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(selectseatLayout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addGroup(selectseatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(selectseatLayout.createSequentialGroup()
                        .addComponent(jLabel29)
                        .addGap(30, 30, 30)
                        .addComponent(jLabel30)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(selectseatLayout.createSequentialGroup()
                        .addComponent(jButton28)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton12)
                        .addGap(95, 95, 95))))
            .addGroup(selectseatLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
            .addGroup(selectseatLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel31)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel32)
                .addGap(173, 173, 173))
        );
        selectseatLayout.setVerticalGroup(
            selectseatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(selectseatLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel32)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel31)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addGroup(selectseatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(jLabel30))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 135, Short.MAX_VALUE)
                .addGroup(selectseatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton12)
                    .addComponent(jButton28))
                .addGap(123, 123, 123))
        );

        jPanel4.add(selectseat, "card14");

        jTable7.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Flight number", "Departure", "Destination", "Airliner", "Take off"
            }
        ));
        jScrollPane7.setViewportView(jTable7);

        jLabel23.setText("Departure");

        jLabel24.setText("Destination");

        jLabel25.setText("Airliner");

        jLabel26.setText("Soonest time:");

        jLabel27.setText("Latest time:");

        jButton26.setText("Search");
        jButton26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton26ActionPerformed(evt);
            }
        });

        jButton27.setText("Next");
        jButton27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton27ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout BookLayout = new javax.swing.GroupLayout(Book);
        Book.setLayout(BookLayout);
        BookLayout.setHorizontalGroup(
            BookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BookLayout.createSequentialGroup()
                .addGroup(BookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(BookLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(BookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(BookLayout.createSequentialGroup()
                                .addComponent(jLabel25)
                                .addGap(53, 53, 53)
                                .addComponent(air3))
                            .addGroup(BookLayout.createSequentialGroup()
                                .addComponent(jLabel24)
                                .addGap(35, 35, 35)
                                .addComponent(des3, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(BookLayout.createSequentialGroup()
                                .addComponent(jLabel23)
                                .addGap(47, 47, 47)
                                .addComponent(dep3, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                        .addGroup(BookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(BookLayout.createSequentialGroup()
                                .addComponent(jLabel27)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ht))
                            .addGroup(BookLayout.createSequentialGroup()
                                .addComponent(jLabel26)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lt, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(BookLayout.createSequentialGroup()
                        .addGap(99, 99, 99)
                        .addComponent(jButton26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton27)
                        .addGap(12, 12, 12)))
                .addGap(116, 116, 116))
            .addGroup(BookLayout.createSequentialGroup()
                .addGroup(BookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(BookLayout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(BookLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel28)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        BookLayout.setVerticalGroup(
            BookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BookLayout.createSequentialGroup()
                .addContainerGap(27, Short.MAX_VALUE)
                .addComponent(jLabel28)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(BookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(jLabel26)
                    .addComponent(dep3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(BookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(jLabel27)
                    .addComponent(des3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ht, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(BookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(air3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(63, 63, 63)
                .addGroup(BookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton26)
                    .addComponent(jButton27))
                .addGap(78, 78, 78))
        );

        jPanel4.add(Book, "card13");

        jTable6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Username", "ID"
            }
        ));
        jScrollPane6.setViewportView(jTable6);

        jButton24.setText("View Personal Information");

        jButton25.setText("Book reservation");
        jButton25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton25ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout CustomerMainLayout = new javax.swing.GroupLayout(CustomerMain);
        CustomerMain.setLayout(CustomerMainLayout);
        CustomerMainLayout.setHorizontalGroup(
            CustomerMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CustomerMainLayout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(CustomerMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(CustomerMainLayout.createSequentialGroup()
                        .addComponent(jButton24)
                        .addGap(140, 140, 140)
                        .addComponent(jButton25))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(58, Short.MAX_VALUE))
        );
        CustomerMainLayout.setVerticalGroup(
            CustomerMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CustomerMainLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(112, 112, 112)
                .addGroup(CustomerMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton24)
                    .addComponent(jButton25))
                .addGap(146, 146, 146))
        );

        jPanel4.add(CustomerMain, "card5");

        jLabel1.setFont(new java.awt.Font("宋体", 0, 18)); // NOI18N
        jLabel1.setText("Log in");

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel2.setText("User:");

        jLabel3.setText("Password:");

        jButton1.setText("Log in");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout loginPanelLayout = new javax.swing.GroupLayout(loginPanel);
        loginPanel.setLayout(loginPanelLayout);
        loginPanelLayout.setHorizontalGroup(
            loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loginPanelLayout.createSequentialGroup()
                .addGroup(loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(loginPanelLayout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addGroup(loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addGap(39, 39, 39)
                        .addGroup(loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(key, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(loginPanelLayout.createSequentialGroup()
                        .addGap(147, 147, 147)
                        .addGroup(loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jButton1))))
                .addContainerGap(313, Short.MAX_VALUE))
        );
        loginPanelLayout.setVerticalGroup(
            loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loginPanelLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel1)
                .addGap(28, 28, 28)
                .addGroup(loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(36, 36, 36)
                .addGroup(loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(key, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.add(loginPanel, "card2");

        jSplitPane2.setRightComponent(jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       String name=jComboBox1.getSelectedItem().toString();
       String key1=key.getText();
      if(ud.checkaccount(name, key1)){
          if(ud.getPre().equals("admin")){
       JOptionPane.showMessageDialog(null,"You can manage now!");
       jButton2.setEnabled(true);
      jButton3.setEnabled(true);
      jButton4.setEnabled(true);
      jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(jPanel1); 
     jPanel4.repaint();
     jPanel4.revalidate(); }else{
          JOptionPane.showMessageDialog(null,"You can manage now \n But not all function"
                  + " are accessible beacause you are not an administrator!");
       jButton2.setEnabled(true);
      jButton3.setEnabled(true);
      jButton4.setEnabled(true);
      jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(jPanel1); 
     jPanel4.repaint();
     jPanel4.revalidate(); }
      }else JOptionPane.showMessageDialog(null,"Wrong password!");
              // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void startbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startbActionPerformed
             jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(loginPanel); 
     jPanel4.repaint();
     jPanel4.revalidate(); // TODO add your handling code here:
    }//GEN-LAST:event_startbActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(AirlinerMain); 
      DefaultTableModel dtm=(DefaultTableModel)jTable1.getModel();
dtm.setRowCount(0);
for(Airliner a : ad.getAd()){
Object row[]=new Object[3];
row[0]=a;
row[1]=a.getPhone();
row[2]=a.getCountry();
dtm.addRow(row);
     jPanel4.repaint();
     jPanel4.revalidate();   // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
       
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        DefaultTableModel dtm=(DefaultTableModel)jTable2.getModel();
dtm.setRowCount(0);
for(Airliner a : ad.getAd()){
    for(Flight f1: a.getFl().getFl()){
Object row[]=new Object[5];
row[0]=f1;
row[1]=a.getName();
row[2]=f1.getAirport();
row[3]=f1.getDestination();
row[4]=df.format(f1.getTakeoff());
dtm.addRow(row);}
}// TODO add your handling code here:
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
            jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(travelagency); 
     jPanel4.repaint();
     jPanel4.revalidate(); 
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
         int srow=jTable1.getSelectedRow();
         Airliner a1=(Airliner)jTable1.getValueAt(srow, 0);
      if(srow>=0){     
          jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
      jPanel4.add(FlightMain); 
           DefaultTableModel dtm=(DefaultTableModel)jTable3.getModel();
dtm.setRowCount(0);
for(Flight f : a1.getFl().getFl()){
Object row[]=new Object[5];
row[0]=f;
row[1]=f.getFleetno();
row[4]=df.format(f.getTakeoff());
row[2]=f.getAirport();
row[3]=f.getDestination();
dtm.addRow(row);}
     FlightMain.repaint();
     FlightMain.revalidate();
      }
      else JOptionPane.showMessageDialog(null,"Please select a row!"); 
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
   jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(AirlinerMain); 
     jPanel4.repaint();
     jPanel4.revalidate();         // TODO add your handling code here:
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
    if( ud.getPre().equals("admin")){   int srow=jTable3.getSelectedRow();
      if(srow>=0){
          Flight f=(Flight)jTable3.getValueAt(srow, 0);
          jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
      jPanel4.add(Update); 
          DefaultTableModel dtm=(DefaultTableModel)jTable4.getModel();
dtm.setRowCount(0);
Object row[]=new Object[5];
row[0]=f;
row[1]=Integer.toString(f.getFleetno());
row[2]=f.getAirport();
row[3]=f.getDestination();
row[4]=df.format(f.getTakeoff());
dtm.addRow(row);
     des.setText(f.getDestination());
     fleetno.setText(Integer.toString(f.getFleetno()));
     flightno.setText(f.getFlightno());
     dep.setText(f.getAirport());
     takeoff.setText(df.format(f.getTakeoff()));
     jPanel4.repaint();
     jPanel4.revalidate();
          
      } else  JOptionPane.showMessageDialog(null,"Please select a row!"); }
    else JOptionPane.showMessageDialog(null,"You don't have right to do that now!");
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
    Flight f=(Flight)jTable4.getValueAt(0, 0);
    jPanel4.removeAll();
       jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(FlightMain); 
DefaultTableModel dtm=(DefaultTableModel)jTable3.getModel();
dtm.setRowCount(0);
for(Airliner a : ad.getAd()) 
            for(Flight f1: a.getFl().getFl()){
Object row[]=new Object[5];
row[0]=f1;
row[1]=f1.getFleetno();
row[2]=df.format(f1.getTakeoff());
row[3]=f1.getAirport();
row[4]=f1.getDestination();
dtm.addRow(row);}
     jPanel4.repaint();
     jPanel4.revalidate();
// TODO add your handling code here:
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
       Flight f=(Flight)jTable4.getValueAt(0, 0);    
        if(flightno.getText().equals("")||fleetno.getText().equals("")||takeoff.getText().equals("")||
                dep.getText().equals("")||des.getText().equals("")) JOptionPane.showMessageDialog(null,"The"
                        + "text field shall not be blank!");
        else if(!isNumeric(fleetno.getText())) JOptionPane.showMessageDialog(null,"Fleet number should"
                + " be an integer!");
        else if(!parseDate(takeoff.getText()))JOptionPane.showMessageDialog(null,"The format of take-off"
                + " time is incorrect!\nIt goes as \" yyyy-MM-dd HH:mm\".");
        else { for(Airliner a : ad.getAd()) 
            for(Flight f1: a.getFl().getFl())
            if (f1.getFlightno()==f.getFlightno()){
            f1.setAirport(dep.getText());
        f1.setDestination(des.getText());
        f1.setFleetno(Integer.parseInt(fleetno.getText()));
            try {
                f1.setTakeoff(df.parse(takeoff.getText()));
            } catch (ParseException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            f1.setFlightno(flightno.getText());
            JOptionPane.showMessageDialog(null,"Update suceed!");
              DefaultTableModel dtm=(DefaultTableModel)jTable4.getModel();
dtm.setRowCount(0);
Object row[]=new Object[5];
row[0]=f1;
row[1]=Integer.toString(f1.getFleetno());
row[2]=f1.getAirport();
row[3]=f1.getDestination();
row[4]=df.format(f1.getTakeoff());
dtm.addRow(row);}
                    
        
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton14ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        if(ud.getPre().equals("admin")){
             Flight f=(Flight)jTable3.getValueAt(0, 0);
             String s="";
        for(Airliner a1:ad.getAd()){
            for(Flight f1:a1.getFl().getFl()){ if(f.getFlightno().equals(f1.getFlightno()))  s=s.concat(a1.getName());}
        }
            jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(addFlight); 
     getnameTX.setText(s);
     jPanel4.repaint();
     jPanel4.revalidate();   
            
        }
        else JOptionPane.showMessageDialog(null,"You don't have right to do that now!");    
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
           jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(searchforTA); 
     wh=0;
     jPanel4.repaint();
     jPanel4.revalidate();   
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
           jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     wh=1;
     jPanel4.add(searchforTA); 
     jPanel4.repaint();
     jPanel4.revalidate();    // TODO add your handling code here:
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton17ActionPerformed
      String flight=flight1.getText();
      int fleet=Integer.parseInt(fleet1.getText());
      String des=des1.getText();
      String dep=dep1.getText();
      
   for(Airliner a : ad.getAd()) 
          if(a.getName().equals(getnameTX.getText())){
        if(flight1.getText().equals("")||fleet1.getText().equals("")||take1.getText().equals("")||
                dep1.getText().equals("")||des1.getText().equals("")) JOptionPane.showMessageDialog(null,"The"
                        + "text field shall not be blank!");
        else if(!isNumeric(fleet1.getText())) JOptionPane.showMessageDialog(null,"Fleet number should"
                + " be an integer!");
        else if(!parseDate(take1.getText()))JOptionPane.showMessageDialog(null,"The format of take-off"
                + " time is incorrect!\nIt goes as \" yyyy-MM-dd HH:mm\".");
        else { 
            Flight f1=a.getFl().addFlight();
            f1.setAirport(dep1.getText());
        f1.setDestination(des1.getText());
        f1.setFleetno(Integer.parseInt(fleet1.getText()));
            try {
                f1.setTakeoff(df.parse(take1.getText()));
            } catch (ParseException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            f1.setFlightno(flight1.getText());
            JOptionPane.showMessageDialog(null,"Add suceed!");
            }
            }
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton17ActionPerformed

    private void jButton16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton16ActionPerformed
         jPanel4.removeAll();
       jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(FlightMain); 
DefaultTableModel dtm=(DefaultTableModel)jTable3.getModel();
dtm.setRowCount(0);
  for(Airliner a : ad.getAd()) 
          if(a.getName().equals(getnameTX.getText())){
          for(Flight f1:a.getFl().getFl()){              
Object row[]=new Object[5];
row[0]=f1;
row[1]=f1.getFleetno();
row[2]=df.format(f1.getTakeoff());
row[3]=f1.getAirport();
row[4]=f1.getDestination();
dtm.addRow(row);}}
     jPanel4.repaint();
     jPanel4.revalidate();
// TODO add your handling code here:
    }//GEN-LAST:event_jButton16ActionPerformed

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
         int count=0;
        String sf1=flight2.getText();
       String dep=dep2.getText();
       String des=des2.getText();
       String airliner1=air.getText();
       ArrayList<Flight> f1=new ArrayList<>();
       for(Airliner a:ad.getAd())  
           if((!airliner1.equals("")&&airliner1.equals(a.getName()))||airliner1.equals(""))
                                for(Flight f:a.getFl().getFl()){                                 
        if(!sf1.equals("")&&!sf1.equals(f.getFlightno()))continue;
        if(!dep.equals("")&&!dep.equals(f.getAirport()))continue;
        if(!des.equals("")&&!des.equals(f.getDestination()))continue;
        f.setAirlinerfornow(a.getName());
        f1.add(f);
        count++;
        }
        if(count==0)JOptionPane.showMessageDialog(null,"There's no correspond flight"
                + " under the given condition");
        else{
            DefaultTableModel dtm=(DefaultTableModel)jTable5.getModel();
dtm.setRowCount(0);
for(Flight f:f1){
Object row[]=new Object[5];
row[0]=f;
row[3]=f.getAirlinerfornow();
row[1]=f.getAirport();
row[2]=f.getDestination();
row[4]=df.format(f.getTakeoff());
dtm.addRow(row);}}
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton19ActionPerformed

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     if(wh==1)
     jPanel4.add(travelagency); 
     else  jPanel4.add(FlightMain); 
     jPanel4.repaint();
     jPanel4.revalidate();  // TODO add your handling code here:
    }//GEN-LAST:event_jButton18ActionPerformed

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed
         if(ud.getPre().equals("admin")){
          int srow=jTable3.getSelectedRow();
       Flight f1=(Flight)jTable3.getValueAt(srow, 0);
      if(srow>=0){  
          int n = JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel this flight"
                  + "?", "Waring!",JOptionPane.YES_NO_OPTION);
         if(n==JOptionPane.YES_OPTION){
             String s="";
         
             for(Airliner a1:ad.getAd())
for(Flight f : a1.getFl().getFl()){if(f.getFlightno().equals(f1.getFlightno())){a1.getFl().getFl().remove(f);
s=s.concat(a1.getName());}}
          jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
      jPanel4.add(FlightMain); 
           DefaultTableModel dtm=(DefaultTableModel)jTable3.getModel();
dtm.setRowCount(0);
 for(Airliner a1:ad.getAd())
     if(a1.getName().equals(s))
for(Flight f : a1.getFl().getFl())
{
Object row[]=new Object[5];
row[0]=f;
row[1]=f.getFleetno();
row[4]=df.format(f.getTakeoff());
row[2]=f.getAirport();
row[3]=f.getDestination();
dtm.addRow(row);}
     FlightMain.repaint();
     FlightMain.revalidate();}
      }
      else JOptionPane.showMessageDialog(null,"Please select a row!");  
            
        }
        else JOptionPane.showMessageDialog(null,"You don't have right to do that now!");    
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton20ActionPerformed

    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
          String name=airname.getText();
      String phone=airphone.getText();
      String country=aircountry.getText();
      
        if(airname.getText().equals("")||airphone.getText().equals("")||aircountry.getText().
                equals("")) JOptionPane.showMessageDialog(null,"The"
                        + "text field shall not be blank!");
        else { 
          Airliner a=ad.addAirliner();
         a.setCountry(country);
         a.setName(name);
         a.setPhone(phone);
            JOptionPane.showMessageDialog(null,"Create suceed!");
            }
            
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton22ActionPerformed

    private void jButton23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton23ActionPerformed
       jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(AirlinerMain); 
      DefaultTableModel dtm=(DefaultTableModel)jTable1.getModel();
dtm.setRowCount(0);
for(Airliner a : ad.getAd()){
Object row[]=new Object[3];
row[0]=a;
row[1]=a.getPhone();
row[2]=a.getCountry();
dtm.addRow(row);
     jPanel4.repaint();
     jPanel4.revalidate(); }
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton23ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
       jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(CustomerMain); 
      DefaultTableModel dtm=(DefaultTableModel)jTable6.getModel();
dtm.setRowCount(0);
for(Customer c:cd.getCd()){
Object row[]=new Object[3];
row[1]=c;
row[0]=c.getAccountname();
dtm.addRow(row);
     jPanel4.repaint();
     jPanel4.revalidate(); } // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton25ActionPerformed
  if(ud.getPre().equals("admin")){
          int srow=jTable6.getSelectedRow();
       Customer c=(Customer)jTable6.getValueAt(srow, 1);
      if(srow>=0){       
          jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
      jPanel4.add(Book); 
      jLabel28.setText(c.getAccountno());
           DefaultTableModel dtm=(DefaultTableModel)jTable7.getModel();
dtm.setRowCount(0);
 for(Airliner a1:ad.getAd())
for(Flight f : a1.getFl().getFl())
{
Object row[]=new Object[5];
row[0]=f;
row[5]=df.format(f.getTakeoff());
row[4]=a1.getName();
row[2]=f.getAirport();
row[3]=f.getDestination();
dtm.addRow(row);}
    jPanel4.repaint();
     jPanel4.revalidate();}
      
      else JOptionPane.showMessageDialog(null,"Please select a row!");  
            
        }
        else JOptionPane.showMessageDialog(null,"You don't have right to do that now!");           
        
// TODO add your handling code here:
    }//GEN-LAST:event_jButton25ActionPerformed

    private void jButton26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton26ActionPerformed
        String dep=dep3.getText();
        String des=des3.getText();
        String air=air3.getText();
        Date dl=new Date();
        Date dh=new Date();
        if(!lt.getText().equals("")){
        try {
            dl=df.parse(lt.getText());
        } catch (ParseException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null,"The format of take-off"
                + " time is incorrect!\nIt goes as \" yyyy-MM-dd HH:mm\".");
        }}
        if(!ht.getText().equals("")){
        try {
           dh=df.parse(ht.getText());
// TODO add your handling code here:
        } catch (ParseException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null,"The format of take-off"
                + " time is incorrect!\nIt goes as \" yyyy-MM-dd HH:mm\".");
        }}
        if(dl.compareTo(dh)>0&&(!lt.getText().equals("")&&!ht.getText().equals("")))
        {JOptionPane.showMessageDialog(null, "The sonnest time should be "
                  + "ealier than the latest time!");}
        else{
        int count=0;
         ArrayList<Flight> f1=new ArrayList<>();
       for(Airliner a:ad.getAd()) 
           if((!air.equals("")&&air.equals(a.getName()))||air.equals(""))
           for(Flight f:a.getFl().getFl()){
           if(!dep.equals("")&&!dep.equals(f.getAirport()))continue;                               
        if(!des.equals("")&&!des.equals(f.getDestination()))continue;
       if(!lt.getText().equals("")&&(dl.compareTo(f.getTakeoff())>0))continue;
       if(!ht.getText().equals("")&&(dh.compareTo(f.getTakeoff())<0))continue;
       f.setAirlinerfornow(a.getName());
        f1.add(f);
        count++;
        }
        if(count==0)JOptionPane.showMessageDialog(null,"There's no correspond flight"
                + " under the given condition");
        else{
            DefaultTableModel dtm=(DefaultTableModel)jTable7.getModel();
dtm.setRowCount(0);
for(Flight f:f1){
Object row[]=new Object[5];
row[0]=f;
row[3]=f.getAirlinerfornow();
row[1]=f.getAirport();
row[2]=f.getDestination();
row[4]=df.format(f.getTakeoff());
dtm.addRow(row);}}
        } 
    }//GEN-LAST:event_jButton26ActionPerformed

    private void jButton27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton27ActionPerformed
       int srow=jTable7.getSelectedRow();
      if(srow>=0){
              Flight f=(Flight)jTable7.getValueAt(srow, 0);
              Customer c=new Customer();
          SeatCatalog s1=f.getSc();
          s1.setall();
          f.setSc(s1);
          f.setall();
          jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(selectseat); 
       DefaultTableModel dtm=(DefaultTableModel)jTable8.getModel();
dtm.setRowCount(0);
for(Seat s: s1.getSc()){
Object row[]=new Object[4];
row[0]=s;
row[1]=s.getPrice();
row[2]=s.getKind();
row[3]=(s.getStatus()==true)? "Sold":"Remaining";
dtm.addRow(row);}
     jLabel30.setText(Integer.toString(150-f.getSc().getnumber()));
     jLabel31.setText(jLabel28.getText());
     jLabel32.setText(f.getFlightno());
     jPanel4.repaint();
     jPanel4.revalidate();
      } else JOptionPane.showMessageDialog(null,"Please select a row!");
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton27ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
int srow=jTable8.getSelectedRow();
if(srow>=0){  
      String ss= jLabel32.getText();
    Seat s=(Seat)jTable8.getValueAt(srow, 0);
    if(s.getStatus())JOptionPane.showMessageDialog(null,"This seat has been booked "
            + "!\nPlease select another seat!");
    else{
        for(Airliner a:ad.getAd())for(Flight f:a.getFl().getFl())
            if(f.getFlightno().equals(ss)){for(Seat s1:f.getSc().getSc())
 if(s1.getNumber()==s.getNumber())for(Customer c:cd.getCd()){if(c.getAccountno().equals(jLabel31.getText()))
 {  Customer cc=  f.getCd().addCustomer();
 cc.setAccountname(c.getAccountname());
 cc.setAccountno(c.getAccountno());
 s1.getC().setAccountname(c.getAccountname());
 s1.getC().setAccountno(c.getAccountno());
 s1.getC().getSeat().add(Integer.toString(s1.getNumber()));
 s1.getC().getFlight().add(f.getFlightno());
 JOptionPane.showMessageDialog(null,"Book succedd!");
 
 }}
            }
          jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
     jPanel4.add(selectseat); 
       DefaultTableModel dtm=(DefaultTableModel)jTable8.getModel();
dtm.setRowCount(0);
for(Airliner a1:ad.getAd()){
for(Flight f:a1.getFl().getFl()){
    if(f.getFlightno().equals(ss)){
        
    f.setall();
for(Seat s1: f.getSc().getSc()){
Object row[]=new Object[4];
row[0]=s1;
row[1]=s1.getPrice();
row[2]=s1.getKind();
row[3]=(s1.getStatus()==true)? "Sold":"Remaining";
dtm.addRow(row);}
     jLabel30.setText(Integer.toString(150-f.getSc().getnumber()));
     jLabel31.setText(ss);
     jLabel32.setText(f.getFlightno());
     jPanel4.repaint();
     jPanel4.revalidate();}}}
    }}
    else JOptionPane.showMessageDialog(null,"Please select a row!");
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton28ActionPerformed
          jPanel4.removeAll();
     jPanel4.repaint();
     jPanel4.revalidate();
      jPanel4.add(Book); 
      jLabel28.setText(jLabel31.getText());
           DefaultTableModel dtm=(DefaultTableModel)jTable7.getModel();
dtm.setRowCount(0);
 for(Airliner a1:ad.getAd())
for(Flight f : a1.getFl().getFl())
{
Object row[]=new Object[5];
row[0]=f;
row[5]=df.format(f.getTakeoff());
row[4]=a1.getName();
row[2]=f.getAirport();
row[3]=f.getDestination();
dtm.addRow(row);}
    jPanel4.repaint();
     jPanel4.revalidate();
      
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton28ActionPerformed
 
public boolean isNumeric(String str){ 
   Pattern pattern = Pattern.compile("[0-9]*"); 
   Matcher isNum = pattern.matcher(str);
   if( !isNum.matches() ){
       return false; 
   } 
   return true; 
}
public boolean parseDate(String da){       
        boolean ifdate=true;  
        try  
        {  
        Date date = df.parse(da);  
        } catch (Exception e)  
        {  
        ifdate=false;  
        }    
            return ifdate;      
    }  
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel AirlinerMain;
    private javax.swing.JPanel Book;
    private javax.swing.JPanel Create;
    private javax.swing.JPanel CustomerMain;
    private javax.swing.JPanel FlightMain;
    private javax.swing.JPanel Personal;
    private javax.swing.JPanel Update;
    private javax.swing.JPanel addFlight;
    private javax.swing.JTextField air;
    private javax.swing.JTextField air3;
    private javax.swing.JTextField aircountry;
    private javax.swing.JTextField airname;
    private javax.swing.JTextField airphone;
    private javax.swing.JTextField dep;
    private javax.swing.JTextField dep1;
    private javax.swing.JTextField dep2;
    private javax.swing.JTextField dep3;
    private javax.swing.JTextField des;
    private javax.swing.JTextField des1;
    private javax.swing.JTextField des2;
    private javax.swing.JTextField des3;
    private javax.swing.JTextField fleet1;
    private javax.swing.JTextField fleetno;
    private javax.swing.JTextField flight1;
    private javax.swing.JTextField flight2;
    private javax.swing.JTextField flightno;
    private javax.swing.JLabel getnameTX;
    private javax.swing.JTextField ht;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton25;
    private javax.swing.JButton jButton26;
    private javax.swing.JButton jButton27;
    private javax.swing.JButton jButton28;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    private javax.swing.JTable jTable5;
    private javax.swing.JTable jTable6;
    private javax.swing.JTable jTable7;
    private javax.swing.JTable jTable8;
    private javax.swing.JPasswordField key;
    private javax.swing.JPanel loginPanel;
    private javax.swing.JTextField lt;
    private javax.swing.JPanel searchforTA;
    private javax.swing.JPanel selectseat;
    private javax.swing.JButton startb;
    private javax.swing.JTextField take1;
    private javax.swing.JTextField takeoff;
    private javax.swing.JPanel travelagency;
    // End of variables declaration//GEN-END:variables
}
