/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Airliner;

/**
 *
 * @author Administrator
 */
public class Airliner {
    public String name;
    public String phone;
    public String country;
    public FlightList fl;

    public Airliner() {
        fl=new FlightList();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public FlightList getFl() {
        return fl;
    }

    public void setFl(FlightList fl) {
        this.fl = fl;
    }
   
    public String toString(){
    return this.name;}
}
