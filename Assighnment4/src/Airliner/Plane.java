/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Airliner;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public class Plane {
     private String manufactor;
    private int madeyear;
    private String snumber;//serial number 
    private String mnumber;//model number 
    private Date date;//maintenence certifate expire date

    public Plane() {
    }

    public String getManufactor() {
        return manufactor;
    }

    public void setManufactor(String manufactor) {
        this.manufactor = manufactor;
    }

    public int getMadeyear() {
        return madeyear;
    }

    public void setMadeyear(int madeyear) {
        this.madeyear = madeyear;
    }

    public String getSnumber() {
        return snumber;
    }

    public void setSnumber(String snumber) {
        this.snumber = snumber;
    }

    public String getMnumber() {
        return mnumber;
    }

    public void setMnumber(String mnumber) {
        this.mnumber = mnumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
}
