/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Airliner;

import Customer.Customer;

/**
 *
 * @author Administrator
 */
public class Seat {
        private boolean status;//if it's saled
    private double price;
    private String kind;//window, middle,or Aisle
    private int number;//seat number
    private Customer c;
    public Seat() {
        c=new Customer();
    }

    public Customer getC() {
        return c;
    }

    public void setC(Customer c) {
        this.c = c;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
 public String toString(){
 return Integer.toString(this.number);}   
}
