/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Airliner;

import Customer.*;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class Flight {
      private Plane plane;
    private String flightno;
    private Date takeoff;
    private String airport;
    private String destination;
    private int fleetno;
    private SeatCatalog sc;
    private CustomerDirectory cd;
private String airlinerfornow;

public void setall(){
for (Customer c :cd.getCd()){
 for(Seat s: sc.getSc()){
 if (s.getC().getAccountno().equals(c.getAccountno()))s.setStatus(true);
 }
 }
}
    public String getAirlinerfornow() {
        return airlinerfornow;
    }

    public void setAirlinerfornow(String airlinerfornow) {
        this.airlinerfornow = airlinerfornow;
    }

    public Flight() {
        sc=new SeatCatalog();
        cd=new CustomerDirectory();
        plane=new Plane();
    }

    public Plane getPlane() {
        return plane;
    }

    public void setPlane(Plane plane) {
        this.plane = plane;
    }

    public String getFlightno() {
        return flightno;
    }

    public void setFlightno(String flightno) {
        this.flightno = flightno;
    }

    public Date getTakeoff() {
        return takeoff;
    }

    public void setTakeoff(Date takeoff) {
        this.takeoff = takeoff;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getFleetno() {
        return fleetno;
    }

    public void setFleetno(int fleetno) {
        this.fleetno = fleetno;
    }

    public SeatCatalog getSc() {
        return sc;
    }

    public void setSc(SeatCatalog sc) {
        this.sc = sc;
    }

    public CustomerDirectory getCd() {
        return cd;
    }

    public void setCd(CustomerDirectory cd) {
        this.cd = cd;
    }
    
    public String toString(){
    return this.flightno;
    }

}
