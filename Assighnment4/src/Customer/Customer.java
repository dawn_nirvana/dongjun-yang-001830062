/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Customer;

import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class Customer {
    private String accountno;
    private String accountname;
    private Person p;
    private ArrayList<String> flight;
    private ArrayList<String> seat;
    private int balance;

    public Customer() {
        flight=new ArrayList<>();
        seat=new ArrayList<>();
        p=new Person();
    }

    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public Person getP() {
        return p;
    }

    public void setP(Person p) {
        this.p = p;
    }

    public ArrayList<String> getFlight() {
        return flight;
    }

    public void setFlight(ArrayList<String> flight) {
        this.flight = flight;
    }

    public ArrayList<String> getSeat() {
        return seat;
    }

    public void setSeat(ArrayList<String> seat) {
        this.seat = seat;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
    public String toString(){
    return this.accountno;}
}
